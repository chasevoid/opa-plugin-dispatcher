/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher.ie;

import com.monadsolutions.opa.plugins.dispatcher.DispatchingOPAPlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewEnginePlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewEngineRegisterArgs;

public class InterviewEngineDispatchingOPAPlugin extends
    DispatchingOPAPlugin<InterviewEnginePlugin, InterviewEngineRegisterArgs>
    implements InterviewEnginePlugin {

    @Override
    public InterviewEnginePlugin getInstance(InterviewEngineRegisterArgs args) {
        return getDispatchingInstance(args);
    }

    @Override
    protected Class<InterviewEnginePlugin> getPluginClass() {
        return InterviewEnginePlugin.class;
    }

	@Override
	public InterviewEnginePlugin getInstance(InterviewEnginePlugin bean,
			InterviewEngineRegisterArgs args) {
		return bean.getInstance(args);
	}
}