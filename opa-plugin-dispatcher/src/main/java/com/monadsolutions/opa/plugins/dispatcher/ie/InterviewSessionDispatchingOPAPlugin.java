/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher.ie;

import com.monadsolutions.opa.plugins.dispatcher.DispatchingOPAPlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewSessionPlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewSessionRegisterArgs;

public class InterviewSessionDispatchingOPAPlugin extends
    DispatchingOPAPlugin<InterviewSessionPlugin, InterviewSessionRegisterArgs>
    implements InterviewSessionPlugin {

    @Override
    public InterviewSessionPlugin getInstance(InterviewSessionRegisterArgs args) {
        return getDispatchingInstance(args);
    }

    @Override
    protected Class<InterviewSessionPlugin> getPluginClass() {
        return InterviewSessionPlugin.class;
    }

	@Override
	public InterviewSessionPlugin getInstance(InterviewSessionPlugin bean,
			InterviewSessionRegisterArgs args) {
		return bean.getInstance(args);
	}
}