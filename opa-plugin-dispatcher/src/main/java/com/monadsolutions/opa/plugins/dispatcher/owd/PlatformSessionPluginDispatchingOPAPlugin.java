/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher.owd;

import com.monadsolutions.opa.plugins.dispatcher.DispatchingOPAPlugin;
import com.oracle.determinations.web.platform.plugins.PlatformSessionPlugin;
import com.oracle.determinations.web.platform.plugins.PlatformSessionRegisterArgs;

public class PlatformSessionPluginDispatchingOPAPlugin extends
    DispatchingOPAPlugin<PlatformSessionPlugin, PlatformSessionRegisterArgs>
    implements PlatformSessionPlugin {

    @Override
    public PlatformSessionPlugin getInstance(PlatformSessionRegisterArgs args) {
        return getDispatchingInstance(args);
    }

    @Override
    protected Class<PlatformSessionPlugin> getPluginClass() {
        return PlatformSessionPlugin.class;
    }

	@Override
	public PlatformSessionPlugin getInstance(PlatformSessionPlugin bean,
			PlatformSessionRegisterArgs args) {
		return bean.getInstance(args);
	}
}