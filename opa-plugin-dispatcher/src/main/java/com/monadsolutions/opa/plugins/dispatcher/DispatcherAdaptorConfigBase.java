/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;


import com.oracle.util.plugins.Plugin;
import com.oracle.util.plugins.RegisterArgs;

/**
 * Base class that can handle setting of plugins on to a dispatcher and filters out adaptors and the source dispatcher. 
 * 
 * @author Luke
 *
 * @param <T>
 * @param <S>
 */
public abstract class DispatcherAdaptorConfigBase<T extends Plugin, S extends RegisterArgs> {
	
	@PostConstruct
	public void setPlugins() {
		List<? extends T> list = getPlugins();
		
		if ( null != list ) {
			// Filter out adaptors and this dispatcher
			ArrayList<T> filteredList = new ArrayList<T>();
			for ( T plugin : list ) {
				if ( plugin != getDispatcher() && !(plugin instanceof OPAPluginAdaptor) ) {
					filteredList.add(plugin);
				}
			}
			getDispatcher().setPlugins(filteredList);	
		}		
	}
	
	protected abstract DispatchingOPAPlugin<T,S> getDispatcher();
	protected abstract List<? extends T> getPlugins();

}
