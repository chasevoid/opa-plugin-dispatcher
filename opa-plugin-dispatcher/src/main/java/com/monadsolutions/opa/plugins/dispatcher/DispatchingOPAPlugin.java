/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.oracle.util.plugins.Plugin;
import com.oracle.util.plugins.RegisterArgs;

public abstract class DispatchingOPAPlugin<T extends Plugin, S extends RegisterArgs> {

	private static final Logger LOG = Logger.getLogger(DispatchingOPAPlugin.class);
	
    /**
     * Invocation handler for a set of plugin interfaces
     * 
     * @author lstudley
     * 
     */
    static class PluginInvocationHandler implements InvocationHandler {
        private final List<?> plugins;

        PluginInvocationHandler(List<?> plugins) {
            this.plugins = plugins;
        }

        public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
            // Find beans with this interface
            Object returnedObject = null;
            Class<?> sourceInterface = method.getDeclaringClass();
            for (Object plugin : plugins) {
                if (sourceInterface.isInstance(plugin)) {
                    // Found a matching plugin ...

                    if (method.getReturnType() != Void.TYPE
                        && returnedObject != null) {
                    	LOG.debug("Previous plugin has already returned an object - skipping call");
                    } else {
                        // invoke the method on this plugin
                        returnedObject = method.invoke(plugin, args);
                    }

                }
            }

            return returnedObject;
        }
    }

    /**
     * The engine plugins that this context knows about
     */
    private List<? extends T> plugins;

    public List<? extends T> getPlugins() {
        return plugins;
    }

    public void setPlugins(List<? extends T> plugins) {
        this.plugins = plugins;
    }

    public abstract T getInstance(S args);

    /**
     * Given the inernal bean - call get instance on it
     * @param bean
     * @param args
     * @return
     */
    public abstract T getInstance(T bean, S args);

    
    protected abstract Class<T> getPluginClass();

    /**
     * 
     * @param <T>
     *            The plugin class
     * @param <S>
     *            The associated register args object
     * @param clazz
     * @param args
     * @return
     */
    @SuppressWarnings("unchecked")
    public T getDispatchingInstance(S args) {
        if (plugins == null || plugins.isEmpty()) {
            return null;
        }

        final List<T> pluginImpls = new ArrayList<T>(10);
        Set<Class<?>> pluginInterfaces = new HashSet<Class<?>>();

        // Perform the lookup on the available session plugins
        for (T bean : plugins) {
            // Ask this plugin if it wants to contribute ..
            T pluginReturned = getInstance(bean,args);
            if (pluginReturned != null) {
                if (pluginReturned != bean) {
                	LOG.debug("OPA plugin bean is not its own plugin");
                }

                LOG.debug("Found a plugin bean " + pluginReturned);
                pluginImpls.add(pluginReturned);

                // Determine the complete set of appropriate plugin interfaces
                // implemented by this bean
                Set<Class<?>> thisPluginInterfaces =
                    getPluginInterfaces(pluginReturned.getClass(),
                        getPluginClass());

                // Add these to the set of interfaces we will be implementing
                pluginInterfaces.addAll(thisPluginInterfaces);
            }
        }

        T plugin = null;
        if (!pluginInterfaces.isEmpty()) {
            // Construct a proxy based on this
            InvocationHandler handler = new PluginInvocationHandler(plugins);

            ArrayList<Class<?>> al = new ArrayList<Class<?>>(pluginInterfaces);
            plugin =
                (T) Proxy.newProxyInstance(getClass().getClassLoader(),
                    al.toArray(new Class<?>[al.size()]), handler);
        }

        return plugin;
    }

    private Set<Class<?>> getPluginInterfaces(Class<?> objClass,
        Class<?> baseInterface) {

        Set<Class<?>> interfaces = new HashSet<Class<?>>();

        Class<?>[] currIntfs = objClass.getInterfaces();
        for (Class<?> currIntf : currIntfs) {
            if (baseInterface.isAssignableFrom(currIntf)) {
                // This interface is part of the appropriate type hierarchy
                interfaces.add(currIntf);
            }
            // Check parent interfaces for all other implemented interfaces
            interfaces.addAll(getPluginInterfaces(currIntf,baseInterface));
        }

        // Now check the parent classes
        Class<?> superclass = objClass.getSuperclass();
		if (null != superclass && Object.class != superclass ) {
            interfaces.addAll(getPluginInterfaces(objClass.getSuperclass(),
                baseInterface));
        }

        return interfaces;
    }

}
