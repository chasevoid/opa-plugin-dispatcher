/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher.ie;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.monadsolutions.opa.plugins.dispatcher.DispatcherAdaptorConfigBase;
import com.monadsolutions.opa.plugins.dispatcher.DispatchingOPAPlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewSessionPlugin;
import com.oracle.determinations.interview.engine.plugins.InterviewSessionRegisterArgs;

@Configuration @Lazy(false)
public class InterviewSessionPluginsSJC extends DispatcherAdaptorConfigBase<InterviewSessionPlugin,InterviewSessionRegisterArgs> {
	
	@Autowired
	private List<? extends InterviewSessionPlugin> plugins;

	@Bean
	public InterviewSessionOPAPluginAdaptor interviewSessionOPAPluginAdaptor() {
		return new InterviewSessionOPAPluginAdaptor(interviewSessionDispatchingOPAPlugin());
	}
		
	@Bean
	public InterviewSessionDispatchingOPAPlugin interviewSessionDispatchingOPAPlugin() {
		InterviewSessionDispatchingOPAPlugin dispatcher = new InterviewSessionDispatchingOPAPlugin(); 
		return dispatcher;
	}

	@Override
	protected DispatchingOPAPlugin<InterviewSessionPlugin, InterviewSessionRegisterArgs> getDispatcher() {
		return interviewSessionDispatchingOPAPlugin();
	}

	@Override
	protected List<? extends InterviewSessionPlugin> getPlugins() {
		return plugins;
	}

}
