package com.monadsolutions.opa.plugins.dispatcher;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.monadsolutions.opa.plugins.dispatcher.ie.InterviewEnginePluginsSJC;
import com.oracle.determinations.interview.engine.plugins.InterviewEnginePlugin;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {InterviewEnginePluginsSJC.class})
public class InterviewEnginePluginDispatcherTest {
	
	@Autowired
	private List<? extends InterviewEnginePlugin> plugins;
	
	@Test
	public void opaPluginDispatcher() {
		assertEquals(2, plugins.size());
	}

}
