package com.monadsolutions.opa.plugins.dispatcher;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.monadsolutions.opa.plugins.dispatcher.ds.AssessPluginsSJC;
import com.oracle.determinations.server.assess.extensions.AssessPlugin;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AssessPluginsSJC.class})
public class AssessPluginDispatcherTest {
	
	@Autowired
	private List<? extends AssessPlugin> plugins;

	@Test
	public void test() {
		assertEquals(2, plugins.size());
	}

}
