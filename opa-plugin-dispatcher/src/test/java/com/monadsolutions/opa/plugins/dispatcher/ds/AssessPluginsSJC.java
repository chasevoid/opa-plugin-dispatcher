/**
 * Copyright (C) 2014 Monad Solutions Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.monadsolutions.opa.plugins.dispatcher.ds;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.monadsolutions.opa.plugins.dispatcher.DispatcherAdaptorConfigBase;
import com.monadsolutions.opa.plugins.dispatcher.DispatchingOPAPlugin;
import com.oracle.determinations.server.assess.extensions.AssessPlugin;
import com.oracle.determinations.server.assess.extensions.AssessPluginRegisterArgs;

@Configuration @Lazy(false)
public class AssessPluginsSJC extends DispatcherAdaptorConfigBase<AssessPlugin,AssessPluginRegisterArgs> {
	
	@Autowired
	private List<? extends AssessPlugin> plugins;

	@Bean
	public AssessPluginOPAPluginAdaptor assessPluginOPAPluginAdaptor() {
		return new AssessPluginOPAPluginAdaptor(assessPluginDispatchingOPAPlugin());
	}
		
	@Bean
	public AssessPluginDispatchingOPAPlugin assessPluginDispatchingOPAPlugin() {
		AssessPluginDispatchingOPAPlugin dispatcher = new AssessPluginDispatchingOPAPlugin(); 
		return dispatcher;
	}

	@Override
	protected DispatchingOPAPlugin<AssessPlugin, AssessPluginRegisterArgs> getDispatcher() {
		return assessPluginDispatchingOPAPlugin();
	}

	@Override
	protected List<? extends AssessPlugin> getPlugins() {
		return plugins;
	}

}
